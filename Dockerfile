FROM centos:7
MAINTAINER bolonheis@gmail.com

WORKDIR /app
RUN yum install -y  epel-release
RUN yum install -y python36 python36-pip
RUN pip3 install --upgrade pip
RUN pip3 install flask
COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "adivinhaweb.py" ]
