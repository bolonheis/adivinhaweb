import random

from flask import Flask, request

destino_numero = '/tmp'
menor_numero = 1
maior_numero = 10
app = Flask(__name__)


@app.route('/gerar-numero', methods=['GET'])
def gera_numero():
    numero_aleatorio = random.randint(menor_numero, maior_numero)
    print(numero_aleatorio)
    file = open(destino_numero + '/resposta.txt', 'w')
    file.write(str(numero_aleatorio))
    file.close()
    response = "<font color=#0000BB size=+1>Número gerado, pode tentar com <i>{}adivinhar/#</i><BR>" \
               "Onde # é um número entre <b>{}</b> e <b>{}</b></font>".format(request.url_root, menor_numero, maior_numero)
    return response


@app.route('/adivinhar/<chute>', methods=['GET'])
def adivinha_numero(chute):
    file = open(destino_numero + '/resposta.txt', 'r')
    resposta = file.readline()
    if int(chute) == int(resposta):
        return "<font color=#009900><b>Acertou, miserávi</b></font>"
    elif int(chute) < int(resposta):
        return "<font color=#FF0000><b>Errou! Tente um numero maior</b></font>"
    else:
        return "<font color=#FF0000><b>Errou! Tente um numero menor</b></font>"

app.run(host="0.0.0.0", port=8880)
